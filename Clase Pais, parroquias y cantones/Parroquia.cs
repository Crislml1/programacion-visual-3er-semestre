using System.Collections.Generic;

namespace Nueva_carpeta
{
    class Parroquia
    {
        public int ParroquiaId { get; set; }
        public string Nombre { get; set; }
        public int Habitantes { get; set; }
    }
}
