﻿using System;
using System.Collections.Generic;

namespace Nueva_carpeta
{
    class Program
    {
        static void Main(string[] args)
        {
            // var ecuador = new Pais(); --> INFERENCIA
            var pais = new Pais();
            pais.Nombre = "Ecuador";
            
            pais.Provincias = new List<Provincia>();
            var provincia1 = new Provincia(); 
            provincia1.Nombre = "Pichincha";
            var provincia2 = new Provincia(); 
            provincia2.Nombre = "Guayas";
            pais.Provincias = new List<Provincia>(){
                provincia1,
                provincia2
            };

            var canton1 = new Canton();
            canton1.Nombre = "Quito";
            var canton2 = new Canton();
            canton2.Nombre = "Guayaquil";
            provincia1.Cantones = new List<Canton>();
            provincia1.Cantones.Add(canton1);
            provincia2.Cantones.Add(canton2);
            
            var parroquia1 = new Parroquia();
            parroquia1.Nombre = "La Madgalena";
            var parroquia2 = new Parroquia();
            parroquia2.Nombre = "Bolívar";
            parroquia1.Habitantes = 150;
            parroquia2.Habitantes = 250;
            canton1.Parroquias = new List<Parroquia>();
            canton1.Parroquias.Add(parroquia1);
            canton2.Parroquias.Add(parroquia2);

             Console.WriteLine($"El restultado de los habitantes totales de las 2 parroquias es: {pais.ObtenerNumeroHabitantes()}");
        }
    }
}
