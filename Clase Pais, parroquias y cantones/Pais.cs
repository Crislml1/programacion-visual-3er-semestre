
using System.Collections.Generic;

namespace Nueva_carpeta
{
    class Pais
    {
        public int PaisId { get; set; }
        public string Nombre { get; set; }
        public List<Provincia> Provincias { get; set; }

        public int ObtenerNumeroHabitantes()
        {
            int acumulador = 0;
            foreach (Provincia actual in Provincias)
            {
                acumulador = acumulador + actual.ObtenerNumeroHabitantes();
            }
            return acumulador;
        }
    }
}
