using System.Collections.Generic;

namespace Nueva_carpeta
{
    class Provincia
    {
        public int ProvinciaId {get; set;}
        public string Nombre {get; set;}
        public List<Canton> Cantones {get; set;} = new List<Canton>();

        public int ObtenerNumeroHabitantes()
        {
            int acumulador = 0;
            foreach (Canton actual in Cantones)
            {
                acumulador = acumulador + actual.ObtenerNumeroHabitantes();
            }
            return acumulador;
        }
    }
}
