using System.Collections.Generic;

namespace Nueva_carpeta
{
    class Canton
    {
        public string CantonId {get; set;}
        public string Nombre {get; set;}
        public List<Parroquia> Parroquias{get; set;} = new List<Parroquia>();

        public int ObtenerNumeroHabitantes()
        {
            int acumulador = 0;
            foreach (Parroquia actual in Parroquias)
            {
                acumulador = acumulador + actual.Habitantes;
            }
            return acumulador;
        }
    }
}
