﻿using System;

namespace VideoJuegos
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length > 0)
                Console.WriteLine("Hola {0}, {1} ", args[0], args[1] + "\n");
            else
            {
                Console.WriteLine("Hola Desconocido");
            }

            //PREGUNTAS
            Console.WriteLine("Bienvenido a nuestra encuesta :). Responde a estas preguntas para saber mas de tus gustos." + "\n");

            Console.WriteLine("Empecemos con algo de Video Juegos.¿Cuál es el nombre de tu Video Juego favorito?" + "\n");
            string nombreJuego = Console.ReadLine() + "\n";

            Console.WriteLine("¿Qué genero es tu Video juego favorito?" + "\n");
            string genero = Console.ReadLine() + "\n";

            Console.WriteLine("¿En que consola juegas tu Video Juego favorito?" + "\n");
            string consola = Console.ReadLine() + "\n";

            var jugador = new juego
            {
                nombreJuego = nombreJuego,
                genero = genero,
                consola = consola
            };

            //RESPUESTAS 
            Console.WriteLine("Wow, tienes los mismos gustos míos :D. Presiona Enter para ver tus respuestas" + "\n");
            Console.WriteLine($"Juego Favorito: {jugador.nombreJuego}" + "\n");
            Console.WriteLine($"Genero de tu juego: {jugador.genero}" + "\n");
            Console.WriteLine($"Consola en la que juegas: {jugador.consola}" + "\n");
            Console.WriteLine("Sigamos con el tema de Animes :). Presiona Enter para continuar" + "\n");
            Console.ReadKey();

            //PREGUNTAS
            Console.WriteLine("¿Te gustan los animes antiguos o nuevos?" + "\n");
            string gustosAnime = Console.ReadLine() + "\n";

            Console.WriteLine("¿Cuántos animes a la semana ves?" + "\n");
            int animeSemanal = int.Parse(Console.ReadLine() + "\n");

            Console.WriteLine("¿Qué genero de anime te gusta más?" + "\n");
            string generoAnime = Console.ReadLine() + "\n";

            Console.WriteLine("¿Por qué te gusta ver anime?" + "\n");
            string aficionAnime = Console.ReadLine() + "\n";

            Console.WriteLine("¿Te sienes identificado con algun anime que hayas visto? Dime el nombre de ese anime" + "\n");
            string vidaAnime = Console.ReadLine() + "\n";

            var espectadorAnime = new anime
            {
                gustosAnime = gustosAnime,
                animeSemanal = animeSemanal,
                generoAnime = generoAnime,
                aficionAnime = aficionAnime,
                vidaAnime = vidaAnime
            };

            //RESPUESTAS   
            Console.WriteLine("Genial, si no fuera una máquina seariamos mejores amigos :D. Presiona Enter para ver tus respuestas" + "\n");
            Console.WriteLine($"Tu gusto de anime con respecto al tiempo: {espectadorAnime.gustosAnime}" + "\n");
            Console.WriteLine($"Los animes que ves a la semana: {espectadorAnime.animeSemanal}" + "\n");
            Console.WriteLine($"Genero favorito de anime: {espectadorAnime.generoAnime}" + "\n");
            Console.WriteLine($"Razon de ver anime: {espectadorAnime.aficionAnime}" + "\n");
            Console.WriteLine($"El anime con el que te sientes identificado: {espectadorAnime.vidaAnime}" + "\n");

            Console.WriteLine("Sigamos con el tema de Comics :). Presiona Enter para continuar" + "\n");
            Console.ReadKey();

            //PREGUNTAS
            Console.WriteLine("Si te gustan los Comics debes saber que existen dos grandes Universos de SuperHeroes; DC Comics y Marvel. ¿Cuál de estos Comics te gustan leer?" + "\n");
            string generoComics = Console.ReadLine() + "\n";

            Console.WriteLine("Respecto a tu respuesta anterior, ¿Cuál es tu superheroe favorito de ese Universo?" + "\n");
            string heroeFavorito = Console.ReadLine() + "\n";

            Console.WriteLine("Si tuvieras la oportunidad un superpoder, ¿Cuál elegirías y por qué?" + "\n");
            string poderFavorito = Console.ReadLine() + "\n";

            Console.WriteLine("¿Cuál es tu comic favorito de tu Universo favorito de Heroes?" + "\n");
            string comicFavorito = Console.ReadLine() + "\n";

            Console.WriteLine("¿En qué fecha se lanzó tu comic favorito?" + "\n");
            string fechaLanzamientoComic = Console.ReadLine() + "\n";
            DateTime fecha = DateTime.Parse(fechaLanzamientoComic);

            Console.WriteLine("¿En dónde comprabas tus Comcis cuando eras pequeño?" + "\n");
            string compraComic = Console.ReadLine() + "\n";

            Console.WriteLine("Aproximadamente, ¿Cuánto te costaba un comic?" + "\n");
            float costoComic = float.Parse(Console.ReadLine() + "\n");

            var lectorComics = new comics
            {
                generoComics = generoComics,
                heroeFavorito = heroeFavorito,
                poderFavorito = poderFavorito,
                comicFavorito = comicFavorito,
                fechaLanzamientoComic = fecha,
                compraComic = compraComic,
                costoComic = costoComic
            };

            //RESPUESTAS
            Console.WriteLine("¡Fantástico! Estamos por terminar la encuesta :D. Presiona Enter para ver tus respuestas." + "\n");
            Console.WriteLine($"Unvierso del Comic favorito: {lectorComics.generoComics}" + "\n");
            Console.WriteLine($"Tu herore favorito: {lectorComics.heroeFavorito}" + "\n");
            Console.WriteLine($"Tu Super poder favorito: {lectorComics.poderFavorito}" + "\n");
            Console.WriteLine($"Tu comic favorito: {lectorComics.comicFavorito}" + "\n");
            Console.WriteLine($"Fecha de lanzamiento de tu comic favorito: {lectorComics.comicFavorito}" + "\n");
            Console.WriteLine($"Lugar en donde comprabas tus comics: {lectorComics.compraComic}" + "\n");
            Console.WriteLine($"Costo de tus comics: {lectorComics.costoComic}" + "\n");

            Console.WriteLine("Este es el ultimo tema de la encuesta. El tema es sobre Paises su cultura. Presiona Enter para continuar " + "\n");
            Console.ReadKey();

            //PREGUNTAS
            Console.WriteLine("Empecemos. ¿En qué país vives actualmente?" + "\n");
            string paisActual = Console.ReadLine() + "\n";

            Console.WriteLine("¿Te gusta la cultura de tu pais? ¿Por qué?");
            string culturaPaisActual = Console.ReadLine() + "\n";

            Console.WriteLine("¿Has viajado a algun otro pais?" + "\n");
            bool viajePais = Console.ReadLine().ToUpper() == "si" + "\n";

            Console.WriteLine("¿A que país te gustaria viajar?" + "\n");
            string viajarOtroPais = Console.ReadLine() + "\n";

            Console.WriteLine("Con respecto a tu respuestua anterior, ¿Por qué te gustaria viajar a ese pais?" + "\n");
            string razonViajarPais = Console.ReadLine() + "\n";

            Console.WriteLine("¿Qué cultura de otro país es la que mas te llama la atención?" + "\n");
            string culturaOtroPais = Console.ReadLine() + "\n";

            var viajero = new viajes
            {
                paisActual = paisActual,
                culturaPaisActual = culturaPaisActual,
                viajePais = viajePais,
                viajarOtroPais = viajarOtroPais,
                razonViajarPais = razonViajarPais,
                culturaOtroPais = culturaOtroPais,
            };

            //RESPUESTAS
            Console.WriteLine("Hemos terminado con las encuestas :). Presiona Enter para ver tus respuestas" + "\n");
            Console.WriteLine($"Pais en el que vives: {viajero.paisActual}" + "\n");
            Console.WriteLine($"La cultura de tu país: {viajero.culturaPaisActual}" + "\n");
            Console.WriteLine($"Has viajado a otro país: {viajero.viajePais}" + "\n");
            Console.WriteLine($"País al que te gustaria viajar: {viajero.viajarOtroPais}" + "\n");
            Console.WriteLine($"La razón de viajar a ese país: {viajero.razonViajarPais}" + "\n");
            Console.WriteLine($"País del que te gusta su cultura: {viajero.culturaOtroPais}" + "\n");

            //DESPEDIDA

            Console.WriteLine("Hemos acabado con todas las preguntas de la encuesta. Muchas gracias por ayudarme con estas preguntas, logré aprender los gustos de otras personas :D espero volver a contar contigo. Adios :3" + "\n");
            Console.ReadKey();
        }
    }
}
