﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationTest.Data;
using WebApplicationTest.Entities;
using WebApplicationTest.Models;

namespace WebApplicationTest.Controllers
{
    public class ConsultasController : Controller
    {
        readonly ApplicationDbContext _context;
        public ConsultasController(ApplicationDbContext context)
        {
            _context = context;
        }

        // API => Application Program Interface
        public IEnumerable<EmpleadoSimple> Empleados()
        {
            IQueryable<Empleado> consulta = _context.Empleados.Include(e => e.Cursos);

            List<Empleado> empleados = consulta.ToList();

            return empleados.Select(
                e => e.ConvertirEnEmpleadoSimple(e));
        }


        public IEnumerable<EmpleadoSimple> EmpleadosPaginados(int pagina, int tamanio)
        {
            // 1, 10 = 1-10
            // 2, 10 = 11-20
            var salto = (pagina - 1) * tamanio;
            var empleados = _context.Empleados.Skip(salto).Take(tamanio).ToList();

            return empleados.Select(e => e.ConvertirEnEmpleadoSimple(e));
        }

        public Empleado[] Data()
        {
            var resultado = from x in _context.Empleados
                            where x.Apellido == "Vasco"
                            select x;
            return resultado.ToArray();

            var resultado1 = _context.Empleados.Where(x => x.Apellido == "Vasco");
            //return resultado1.ToArray();
        }

        public object Ejemplo1()
        {
            var consulta = from e in _context.Empleados
                           from h in _context.Hijos
                           where e.EmpleadoId == h.EmpleadoId
                           select e;
            return consulta.ToList();

            var consulta0 = from e in _context.Empleados
                            join h in _context.Hijos
                            on e.EmpleadoId equals h.EmpleadoId
                            select new { e.Apellido, NumeroHijos = e.Hijos.Count };

            var consulta1 = from e in _context.Empleados
                            from h in _context.Hijos
                            where e.EmpleadoId == h.EmpleadoId
                            select new { e.Apellido, NumeroHijos = e.Hijos.Count };
            return consulta1.ToArray();

            var consulta2 = _context.Empleados
                .Include(e => e.Hijos)
                .Select(e => new { e.Apellido, NumeroHijos = e.Hijos.Count});
        }

        public Empleado Crear ([Bind("EmpleadoId,Salario,Nombre,Apellido,DepartamentoId")] Empleado empleado)
        {
            _context.Add(empleado);
            _context.SaveChanges();

            _context.Entry(empleado).Collection(e => e.Hijos).Load();
            _context.Entry(empleado).Reference(e => e.Departamento).Load();

            //LazyLoad => solo trae una clase (Empleado). Solo es una carga
            //EagerLoad => trae toda a informacio cargada
            //ExplicityLoad  
            return empleado;
        }

        public void OtroEjemplo()
        {
            using (var otroContexto = new ApplicationDbContext( null ))
            {   
                if (otroContexto.Cursos.Any(c => c.NumeroCreditos > 100))
                {
                    //TODO: que bueno!
                }
            }
        }
    }
}

