﻿using Microsoft.EntityFrameworkCore;
using WebApplicationOne.Entities;

namespace WebApplicationOne.Data
{
    public class MainDbContext : DbContext
    {
        public DbSet<Mountain> Mountains { get; set; }
        public MainDbContext(DbContextOptions<MainDbContext> dbContextOptions): base(dbContextOptions)
        {
        }

        //protected override void OnConfiguring(DbContextOptionsBuilder options)
          //  => options.UseSqlServer(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Mountain;Integrated Security=True;");
    }
}
