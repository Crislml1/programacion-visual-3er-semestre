﻿
using System.ComponentModel;

namespace WebApplicationOne.Entities
{
    public class Mountain
    {
        public int MountainId { get; set; }
        [DisplayName("Nombre de la montaña")]
        public string Nombre { get; set; }
        [DisplayName("Altitud en metros")]
        public int Altitud { get; set; }
    }
}
