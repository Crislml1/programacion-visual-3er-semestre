﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationOne.Controllers
{
    public class CalculosController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        // TODO: como acceder a Request.Form asp.net core 
        [HttpPost]
        public IActionResult Sumar(int primerNumero, int segundoNumero)
        {
            var primerNumeroFromBody = int.Parse(Request.Form["primerNumero"]);
            var segundoNumeroFromBody = int.Parse(Request.Form["segundoNumero"]);
            var resultadoFromBody = primerNumero + segundoNumero;

            var resultado = primerNumero + segundoNumero;

            ViewData["Resultado"] = resultado;
            ViewData["ResultadoFromBody"] = resultadoFromBody;
            
            return View();
        }
    }
}
