﻿namespace TareaInyeccion
{
    public interface IHeroe
    {
        void Presentar();
    }
}
