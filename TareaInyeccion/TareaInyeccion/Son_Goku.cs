﻿using System;

namespace TareaInyeccion
{
    class Son_Goku : IHeroe
    {
        public void Presentar()
        {
            Console.WriteLine("\nHola, soy Goku. El saiyajin mas fuerte de la tierra.");
        }
    }
}
