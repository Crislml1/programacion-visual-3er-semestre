﻿using System;

namespace TareaInyeccion
{
    class Cell : IVillano
    {
        public void Responder()
        {
            Console.WriteLine("\nSoy Cell, el enemigo mas poderoso del unvierso y el mejor androide que ha creado el doctor Gero");
        }
    }
}
