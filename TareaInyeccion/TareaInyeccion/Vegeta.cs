﻿using System;

namespace TareaInyeccion
{
    class Vegeta : IHeroe
    {
        public void Presentar()
        {
            Console.WriteLine("\nSoy el Principe Vegeta, un gerreo de élite. ¡Si no quires desaparecer entones sal de mi vista insecto!");
        }
    }
}
