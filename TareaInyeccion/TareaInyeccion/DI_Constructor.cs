﻿using System;

namespace TareaInyeccion
{
    class DI_Constructor
    {
        public static void Iniciar()
        {
            IHeroe goku = new Son_Goku();
            Heroe heroe1 = new Heroe(goku);
            heroe1.Presentar();

            IHeroe vegeta = new Vegeta();
            Heroe heroe2 = new Heroe(vegeta);
            heroe2.Presentar();

            //Console.ReadLine();
        }
    }
}
