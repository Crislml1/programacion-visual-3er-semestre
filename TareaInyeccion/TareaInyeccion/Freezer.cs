﻿using System;

namespace TareaInyeccion
{
    class Freezer : IVillano
    {
        public void Responder()
        {
            Console.WriteLine("\nSoy Freezer, emperador del mal y futuro dueño del universo. Derrotare a esos desquiciados saiyajin");
        }
    }
}
