﻿using System;

namespace TareaInyeccion
{
    class DI_Metodo
    {
        public static void Iniciar()
        {
            Villano villano1 = new Villano();
            villano1.Responder(new Freezer());

            Villano villano2 = new Villano();
            villano2.Responder(new Cell());

            Console.ReadLine();
        }
    }
}
