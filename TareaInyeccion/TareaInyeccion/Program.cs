﻿using System;

namespace TareaInyeccion
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("----------Tarea de Inyección----------");
            Console.WriteLine("Christopher Vasco");
            Console.WriteLine("--------------------------------------");
            Console.WriteLine("Inyección de dependencias por Constructor (Heroes: Son Goku y Vegeta)");
            DI_Constructor.Iniciar();
            Console.WriteLine("--------------------------------------");
            Console.WriteLine("Inyeccion de dependencia por Metodo (Villanos: Freezer y Cell)");
            DI_Metodo.Iniciar();

        }   
    }
}
