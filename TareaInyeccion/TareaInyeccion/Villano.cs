﻿namespace TareaInyeccion
{
    class Villano
    {
        public void Responder(IVillano villano)
        {
            villano.Responder();
        }
    }
}