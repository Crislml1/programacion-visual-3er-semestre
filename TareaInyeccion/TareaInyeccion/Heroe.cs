﻿namespace TareaInyeccion
{
    internal class Heroe
    {
        private IHeroe heroe;
        public Heroe(IHeroe heroe)
        {
            this.heroe = heroe;
        }
        public void Presentar()
        {
            heroe.Presentar();
        }
    }
}