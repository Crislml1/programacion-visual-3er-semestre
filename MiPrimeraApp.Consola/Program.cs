﻿using System;

namespace MiPrimeraApp.Consola
{
    class Program
    {
        static void Main(string[] args)
        {
            // pedir 10 datos persona: bool, string, float, int, double, dateTime
            if (args.Length > 0)
                Console.WriteLine("Hola {0}, {1} ", args[0], args[1] + "\n");
            else
            {
                Console.WriteLine("Hola Desconocido");
            }
            Console.WriteLine("Por favor necesito que me ayudes con unos pequeños datos :). Tus datos serán revelados al final de las preguntas." + "\n");

            Console.WriteLine("¡Empezemos! ¿Cuál es tu nombre completo?" + "\n");
            var nombreCompleto = Console.ReadLine() + "\n";
            
            Console.WriteLine("Siguiente pregunta. ¿Cúantos años tienes?" + "\n");
            int edad = int.Parse(Console.ReadLine() + "\n") ;
            
            Console.WriteLine("Siguiente pregunta. ¿En dónde naciste?" + "\n");
            var lugarNacimiento = Console.ReadLine() + "\n";
            
            Console.WriteLine("Siguiente pregunta. ¿Cául es tu número de cedula?" + "\n");
            var cedula = Console.ReadLine() + "\n";
            
            Console.WriteLine("Siguente pregunta. ¿Estudias o trabajas?" + "\n");
            var preparacion = Console.ReadLine() + "\n";
            
            Console.WriteLine("Si tu respuesta fue trabajar; ¿En qué trabajas? Si tu respuesta fue estudiar responde <no trabajo>" + "\n");
            var trabajo = Console.ReadLine() + "\n";
            
            Console.WriteLine("Si tu respuesta fue estudiar; ¿Qué materia es la que más te gusta? Si tu respuesta fue trabajar responde <no estudio>" + "\n");
            var estudio = Console.ReadLine() + "\n";
            
            Console.WriteLine("Siguiente pregunta. ¿Qué tipo de música te gusta?" + "\n");
            var musica = Console.ReadLine() + "\n";
            
            Console.WriteLine("Siguiente pregunta. ¿Cuál es tu comida favorita?" + "\n");
            var comida = Console.ReadLine() + "\n";
            
            Console.WriteLine("Última pregunta. ¿Cuál es tu deporte favorito?" + "\n");
            var deporte = Console.ReadLine() + "\n";
            
            var persona = new Persona();
            persona.nombreCompleto = nombreCompleto;
            persona.edad = edad;
            persona.lugarNacimiento = lugarNacimiento;
            persona.cedula = cedula;
            persona.preparacion = preparacion;
            persona.trabajo = trabajo;
            persona.estudio = estudio;
            persona.musica = musica;
            persona.comida = comida;
            persona.deporte = deporte;
            Console.WriteLine("Hemos acabado con las preguntas. Tus respuestas seran presentadas." + "\n");
            Console.WriteLine($"Nombre completo: {persona.nombreCompleto}" + "\n");
            Console.WriteLine("Edad: " + persona.edad + "\n");
            Console.WriteLine("Lugar de nacimiento: " + persona.lugarNacimiento + "\n");
            Console.WriteLine("Número de cedula: " + persona.cedula + "\n");
            Console.WriteLine("Tu preparacion: " + persona.preparacion + "\n");
            Console.WriteLine("Tu trabajo: " + persona.trabajo + "\n");
            Console.WriteLine("Tu materia favorita: " + persona.estudio + "\n");
            Console.WriteLine("Tu música favorita: " + persona.musica + "\n");
            Console.WriteLine("Tu comida favorita: " + persona.comida + "\n");
            Console.WriteLine("Tu deporte favorita: " + persona.comida + "\n");
            Console.WriteLine("Gracias por responder todas nuestras preguntas. Que tenga un buen día :). Para salir presione la tecla Enter");
            Console.ReadKey();
        }
        void Saludoinicial(string[] args)
        {
            if (args.Length > 0)
                Console.WriteLine("Hola {0}, {1}", args[0], args[1]);
            else
            {
                Console.WriteLine("Hola desconocido");
            }
            Console.WriteLine("¿Cómo estas?");
            var estado = Console.ReadLine();
            Console.WriteLine("Umm... Entiendo");
            Console.ReadLine();
            Console.WriteLine(estado);
        }
    }
}
