namespace MiPrimeraApp.Consola
{
    class Persona
    {
        public string nombreCompleto;
        public int edad;
        public string lugarNacimiento;
        public string cedula;
        public string preparacion;
        public string trabajo;
        public string estudio;
        public string musica;
        public string comida;
        public string deporte;
    }
}

