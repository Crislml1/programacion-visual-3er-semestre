﻿using System;

namespace Tarea2
{
    class Program
    {
        static void Main(string[] args)
        {
            Nodo raiz = new Nodo()
            {
                valor = "*",
                nivel = 0,
                hijos ={
                    new Nodo{
                        valor="/",
                        hijos={
                            new Nodo{
                                valor="1"
                            },
                            new Nodo{
                                valor ="2"
                            },
                            new Nodo{
                                valor ="3"
                            }
                        }
                    },
                    new Nodo{
                        valor ="+",
                        hijos={
                            new Nodo{
                                valor="4"
                            },
                            new Nodo{
                                valor="5"
                            }
                        }
                    },
                    new Nodo{
                        valor="-",
                        hijos={
                            new Nodo{
                                valor="6"
                            },
                            new Nodo{
                                valor="7"
                            }

                        }
                    }
                },
            };

            manejadorArbol controladorArbol = new manejadorArbol();
            Console.WriteLine($"Número de niveles: {controladorArbol.contadorNiveles(raiz)}");
            Console.WriteLine($"Número de hojas: {controladorArbol.contadorHojas(raiz)}");
            Console.WriteLine($"Número de nodos: {controladorArbol.contadorNodos(raiz)}");
        }
    }
}