using System.Collections.Generic;
namespace Tarea2
{
    class Nodo
    {
        public List<Nodo> Nodos { get; set; } = new List<Nodo>();
        public string valor { get; set; }
        public List<Nodo> hijos { get; set; } = new List<Nodo>();
        public int nivel;
        public int nodoRaiz = 0;
    }
}