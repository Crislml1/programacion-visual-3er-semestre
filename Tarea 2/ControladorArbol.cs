using System.Linq;
namespace Tarea2
{
    internal class manejadorArbol
    {
        internal int contadorNiveles(Nodo nodo)
        {
            int acumulador = 0;
            foreach (Nodo actual in nodo.hijos)
            {
                acumulador += actual.valor.Count();
            }
            return acumulador;
        }
        internal int contadorHojas(Nodo nodo)
        {
            int acumulador = 0;
            foreach (Nodo actual in nodo.hijos)
            {
                acumulador += actual.valor.Count() + actual.hijos.Count();
            }
            return acumulador;
        }

        internal int contadorNodos(Nodo nodo)
        {
            int acumulador = 0;
            acumulador += nodo.nodoRaiz + nodo.hijos.Count();
            foreach (Nodo actual in nodo.hijos)
            {
                acumulador += actual.hijos.Count;
            }
            return acumulador;
        }

    }
}